<?php

namespace App\Http\Controllers;
 
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

use App\Jobs\CreateUserJob;

use App\Job;
 
class UsersController extends Controller
{
	/**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    	CreateUserJob::dispatch($request->username, $request->email, $request->password)->onQueue("high");

    	$idJob = Job::select('id')->orderBy('id', 'DESC')->first()->id;

        return response()->json(["id_job" => $idJob]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    	$job = Job::where("id", "=", $id)->first();

    	$estatus = "pending";

    	if(!$job){
    		$estatus = "complete";
    	}

        return response()->json(["estatus_job" => $estatus]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $job = Job::where("id", "=", $id)->first();

        if(!$job){
        	return response()->json(["success" => false]);
        }

        CreateUserJob::dispatch($request->username, $request->email, $request->password)->onQueue("notproccess");

    	$newJob = Job::select('id')->orderBy('id', 'DESC')->first();

    	$newJob = Job::where('id', "=", $newJob->id)->first();

    	$job->payload = $newJob->payload;

    	$job->save();

    	Job::where('id',$newJob->id)->delete();

    	return response()->json(["success" => true]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}